/* This file is in the public domain */
(function () {
  var taler = window["taler"] = window["taler"] || {};
  var gtw;
  var present;
  var presentFns = [];
  var absentFns = [];
  if ("__gnu_taler_wallet" in window) {
    gtw = __gnu_taler_wallet;
    __gnu_taler_wallet.init();
    present = true;
  }
  else {
    gtw = window["__gnu_taler_wallet"] = {};
    present = false;
  }
  taler.onPresent = function (f) {
    presentFns.push(f);
    if (present) {
      f();
    }
  };
  taler.onAbsent = function (f) {
    absentFns.push(f);
    if (!present) {
      f();
    }
  };
  gtw.presentCallback = function() {
    for (var i = 0; i < presentFns.length; i++) {
      presentFns[i]();
    }
    present = true;
  };
  gtw.absentCallback = function() {
    for (var i = 0; i < absentFns.length; i++) {
      absentFns[i]();
    }
    present = false;
  };
})();
