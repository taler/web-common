/* @licstart  The following is the entire license notice for the
  JavaScript code in this page.

  Copyright (C) 2015, 2016 INRIA

  The JavaScript code in this page is free software: you can
  redistribute it and/or modify it under the terms of the GNU
  Lesser General Public License (GNU LGPL) as published by the Free Software
  Foundation, either version 2.1 of the License, or (at your option)
  any later version.  The code is distributed WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU LGPL for more details.

  As additional permission under GNU LGPL version 2.1 section 7, you
  may distribute non-source (e.g., minimized or compacted) forms of
  that code without the copy of the GNU LGPL normally required by
  section 4, provided you include this license notice and a URL
  through which recipients can access the Corresponding Source.

  @licend  The above is the entire license notice
  for the JavaScript code in this page.

  @author Marcello Stanisci
*/
window.onclick = function(e) {
  var dropdowns = document.getElementsByClassName("dropdown-content");
  if (!e.target.matches('.dropbtn')) {
    for (var d = 0; d < dropdowns.length; d++) {
      var openDropdown = dropdowns[d];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
  else{ // need to close other tabs belonging to tabs other than the clicked one 
    for (var d = 0; d < dropdowns.length; d++) {
      var openDropdown = dropdowns[d];
      if ((openDropdown.parentNode != e.target.parentNode)
        && (openDropdown.classList.contains("show"))){
        openDropdown.classList.remove('show');
      }
    }        
  }
}
function injectOnclicks(){
  var where = ["shop", "bank", "landing"];
  for (i in where){
    document.getElementById(where[i]).onclick = function(){
      this.parentNode.children[1].classList.toggle("show");
      }
  }
}
function dropMenu(){

}
document.addEventListener('DOMContentLoaded', injectOnclicks);
